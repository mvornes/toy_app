# README

CHAPTER 2 - TOY APP

TSI - Web 5 - Michael Vornes

## Exercises
#### Section 2.2
#### Section 2.2.1


1 - (For readers who know CSS) Create a new user, then use your browser’s HTML inspector to determine the CSS id for the text “User was successfully created.” What happens when you refresh your browser?

R: O id para o parágrafo com a mensagem "User was successfully created." é "notice". Quando a página é recarregada o parágrafo fica vazio, a mensagem desaparece.

2 - What happens if you try to create a user with a name but no email address?

R: É possível criar um usuário sem endereço de e-mail, não aparece nenhuma mensagem de aviso/validação.

3 - What happens if you try create a user with an invalid email address, like “@example.com”?

R: É possível criar um usuário com endereço de e-mail inválido, não aparece nenhuma mensagem de aviso/validação.

4 - Destroy each of the users created in the previous exercises. Does Rails display a message by default when a user is destroyed?

R: Sim, a mensagem "User was successfully destroyed." é mostrada.


#### Section 2.2.2


1 - By referring to Figure 2.11, write out the analogous steps for visiting the URL /users/1/edit.

R:

a) O navegador faz uma requisição para a URL /users/1/edit.

b) O Rails "routeia" /users/1/edit para ação edit no controller de Users.

c) A ação edit solicita ao model User para trazer o usuário com id 1.

d) O model User puxa o usuário do banco de dados e retorna o usuário para o controller.

f) O controller captura o usuário que é passado para a view edit.

g) A view usa Ruby para rederizar a página como HTML.

h) O controller passa o HTML de volta para o navegador que exibe o formulário para editar


2 - Find the line in the scaffolding code that retrieves the user from the database in the previous exercise.

R:

/app/controller/users_controller.rb

    class UsersController < ApplicationController
      def edit
      end
    end


/app/models/user.rb

    class User < ApplicationRecord
    end


3 - What is the name of the view file for the user edit page?

R: app/views/users/edit.html.erb


#### Section 2.3
#### Section 2.3.1


1 - (For readers who know CSS) Create a new micropost, then use your browser’s HTML inspector to determine the CSS id for the text “Micropost was successfully created.” What happens when you refresh your browser?

R: O id para o parágrafo com a mensagem "Micropost was successfully created." é "notice". Quando a página é recarregada o parágrafo fica vazio, a mensagem desaparece.


2 - Try to create a micropost with empty content and no user id.

R: É possível criar um micropost sem conteúdo e usuários, não aparece nenhuma mensagem de aviso/validação.


3 - Try to create a micropost with over 140 characters of content (say, the first paragraph from the Wikipedia article on Ruby).

R: É possível criar um micropost com mais de 140 caracteres, não aparece nenhuma mensagem de aviso/validação.


4 - Destroy the microposts from the previous exercises.

R: Ok


#### Section 2.3.2


1 - Try to create a micropost with the same long content used in a previous exercise (Section 2.3.1.1). How has the behavior changed?

R: Não é possível salvar o micropost, após validação aparece mensagem de erro indicando que o conteúdo tem mais de 140 caracteres.


2 - (For readers who know CSS) Use your browser’s HTML inspector to determine the CSS id of the error message produced by the previous exercise.

R: error_explanation


#### Section 2.3.3


1 - Edit the user show page to display the content of the user’s first micropost. (Use your technical sophistication (Box 1.1) to guess the syntax based on the other content in the file.) Confirm by visiting /users/1 that it worked.

R:

app/models/micropost.rb

    presence: true


2 - The code in Listing 2.16 shows how to add a validation for the presence of micropost content in order to ensure that microposts can’t be blank. Verify that you get the behavior shown in Figure 2.16.

R: Uma div contém o id error_explanation; Os labels, input e textarea contém a classe field_with_errors.


3 - Update Listing 2.17 by replacing FILL_IN with the appropriate code to validate the presence of name and email attributes in the User model (Figure 2.17).

R:

app/models/user.rb

    validates :name, presence: true
    validates :email, presence: true


#### Section 2.3.4


1 - By examining the contents of the Application controller file, find the line that causes ApplicationController to inherit from ActionController::Base.

R:

    class ApplicationController < ActionController::Base


2 - Is there an analogous file containing a line where ApplicationRecord inherits from ActiveRecord::Base? Hint: It would probably be a file called something like application_record.rb in the app/models directory.

R:

    class ApplicationRecord < ActiveRecord::Base
